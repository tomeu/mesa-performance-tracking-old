#!/usr/bin/python3

# Copyright © 2021 Collabora Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

import argparse
import bz2
import gitlab
import influxdb_client
import json
import logging
import os
import requests
import sys
import time

from datetime import timedelta
from dateutil import parser

GITLAB_URL = os.environ.get('GITLAB_URL', 'https://gitlab.freedesktop.org')
GITLAB_PROJECT_PATH = os.environ.get('GITLAB_PROJECT_PATH', 'mesa/mesa')
GITLAB_TOKEN = os.environ.get('GITLAB_TOKEN', '<token unspecified>')
INFLUX_URL= os.environ.get('INFLUX_URL', 'https://influx-packet.freedesktop.org')
INFLUX_ORG = os.environ.get('INFLUX_ORG', 'Freedesktop')
INFLUX_BUCKET = os.environ.get('INFLUX_BUCKET', 'mesa')
INFLUX_TOKEN = os.environ.get('INFLUX_TOKEN', '<token unspecified>')

def wait_for_pipeline(project, sha):
    while True:
        pipelines = project.pipelines.list(sha=sha)
        if pipelines:
            return pipelines[0]
        time.sleep(2)

def get_trace_jobs(pipeline):
    """ - `pipeline` The pipeline from which to look for trace jobs
    - `return` The jobs of the pipeline responsible for replaying traces"""
    jobs = []

    print("Retrieving jobs for pipeline {}".format(pipeline.id))
    for job in pipeline.jobs.list(as_list=False, per_page=150):
        if job.status == 'created':
            continue
        if "-profile-traces" in job.name:
            jobs.append(job)

    return jobs

def get_results(job):
    """- `return` A Results object with the results artifacts of the specified job"""
    if job.pipeline['status'] == 'running':
        logging.warning("Pipeline {} still running".format(job.pipeline['id']))
        return None

    url = "{}/artifacts/raw/results/results.json.bz2".format(job.web_url)
    print("Requesting results from: {}".format(url))
    r = requests.get(url)

    if r.status_code != 200:
        print("Failed to download ({}):\n\t{}".format(r.status_code, url))
        return None

    return json.loads(bz2.decompress(r.content))

def get_trace_frame_times(results, trace: str):
    """- `return` The frame times of a specified trace within the specified results"""

    if results['tests'][trace]['images'] is None:
        return []

    if not "frame_times" in results['tests'][trace]['images'][0]:
        return []

    frame_times = results['tests'][trace]['images'][0]["frame_times"]

    print("Found {} frame times in {}".format(len(frame_times), trace))

    return frame_times

def write_frame_time(influx, pipeline_time, job_name, trace_name,
                     frame_time, project_path, pipeline_id, commit_sha,
                     commit_title, job_id):
    """- `trace_name` Name of the trace that has been replayed
    - `frame_time` Time for a frame to hit presentation in nanoseconds"""
    p = influxdb_client.Point("frame_time")
    p = p.tag("job_name", job_name)
    p = p.tag("trace_name", trace_name)
    p = p.tag("project_path", project_path)
    p = p.tag("pipeline_id", pipeline_id)
    p = p.tag("commit_sha", commit_sha)
    p = p.tag("commit_title", commit_title)
    p = p.tag("job_id", job_id)
    p = p.field("frame_time", frame_time)
    p = p.time(pipeline_time)
    influx.write(bucket=INFLUX_BUCKET,
                 org=INFLUX_ORG,
                 record=p)

def get_parent_commit(project, sha):
    commit = project.commits.get(sha)
    return project.commits.get(commit.parent_ids[0])

def upload_data(project, influx, pipeline):
    print('Pipeline: %s' % pipeline.web_url)

    # Top commit is the job definitions
    parent_commit = get_parent_commit(project, pipeline.sha)

    for job in get_trace_jobs(pipeline):
        results = get_results(job)

        # Skip jobs with no results
        if results is None:
            continue

        # Get frame times for apitrace traces
        for trace in results['tests']:
            if not trace.endswith(".trace"):
                continue
            frame_times = get_trace_frame_times(results, trace)
            # Ignore last frame, tends to be quite off
            print("Adding %d frames for trace %s from job %s" % (len(frame_times[:-1]), trace, job.name))
            for frame_time in frame_times[:-1]:
                # Update influxDB with current trace
                write_frame_time(influx,
                                 parser.parse(pipeline.created_at),
                                 job.name,
                                 '/'.join(trace.split('@')[2:]),
                                 int(frame_time),
                                 GITLAB_PROJECT_PATH,
                                 pipeline.id,
                                 parent_commit.short_id,
                                 parent_commit.title,
                                 job.id)
        influx.flush()

influx_client = influxdb_client.InfluxDBClient(
    url=INFLUX_URL,
    token=INFLUX_TOKEN,
    org=INFLUX_ORG
)

gl = gitlab.Gitlab(url=GITLAB_URL, private_token=GITLAB_TOKEN)
gl.enable_debug()

project = gl.projects.get(GITLAB_PROJECT_PATH)

if len(sys.argv) > 1:
    pipeline = wait_for_pipeline(project, sys.argv[1])
    with influx_client.write_api() as write_api:
        upload_data(project, write_api, pipeline)
else:
    for pipeline in project.pipelines.list(as_list=False,
                                           per_page=50,
                                           ref='ci-performance-tracking'):
        with influx_client.write_api() as write_api:
            upload_data(project, write_api, pipeline)
