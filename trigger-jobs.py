#!/usr/bin/python3

# Copyright © 2021 Collabora Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

import gitlab
import os
import sys
import time

GITLAB_URL = os.environ.get('GITLAB_URL', 'https://gitlab.freedesktop.org')
GITLAB_PROJECT_PATH = os.environ.get('GITLAB_PROJECT_PATH', 'mesa/mesa')
GITLAB_TOKEN = os.environ.get('GITLAB_TOKEN', '<token unspecified>')

DEPENDENCIES = ['debian/x86_build-base',
                'debian/x86_build',
                'debian/arm_build',
                'debian/arm_test',
                'debian-arm64',
                'debian-armhf',
                'debian-testing',
                'kernel+rootfs_armhf',
                'kernel+rootfs_arm64',
                'kernel+rootfs_amd64',
                ]

def wait_for_pipeline(project, sha):
    while True:
        pipelines = project.pipelines.list(sha=sha)
        if pipelines:
            return pipelines[0]
        print('Waiting for pipeline for SHA %s' % sha)
        time.sleep(2)

def trigger_jobs_for_pipeline(project, pipeline):
    while True:
        running_jobs = []
        for job in pipeline.jobs.list(all=True):
            if job.name in DEPENDENCIES or \
               job.name.endswith('-profile-traces'):
                if job.status == 'manual':
                    print('Triggering job %s' % job.name)
                    pjob = project.jobs.get(job.id, lazy=True)
                    pjob.play()
                    running_jobs.append(job)
                elif job.status not in ['failed', 'canceled', 'success', 'skipped']:
                    running_jobs.append(job)
            else:
                if job.status != 'canceled':
                    print('Canceling job %s' % job.name)
                    pjob = project.jobs.get(job.id, lazy=True)
                    pjob.cancel()
                elif job.status not in ['failed', 'canceled', 'success', 'skipped']:
                    running_jobs.append(job)
        if not running_jobs:
            return
        print('Waiting for jobs to finish: %s' % ', '.join(job.name for job in running_jobs))
        time.sleep(10)

gl = gitlab.Gitlab(url=GITLAB_URL, private_token=GITLAB_TOKEN)
project = gl.projects.get(GITLAB_PROJECT_PATH)

pipeline = wait_for_pipeline(project, sys.argv[1])
print('Pipeline: %s' % pipeline.web_url)
trigger_jobs_for_pipeline(project, pipeline)
