set -x

git config --global user.email $GITLAB_USER_EMAIL
git config --global user.name $GITLAB_USER_NAME

git clone https://gitlab.freedesktop.org/mesa/mesa.git || true
cd mesa

git fetch https://gitlab.freedesktop.org/tomeu/mesa.git ci-performance-tracking
LAST_DOWNSTREAM_COMMIT=$(git log -2 --oneline FETCH_HEAD | tail -1)

git fetch https://gitlab.freedesktop.org/mesa/mesa.git main
LAST_UPSTREAM_COMMIT=$(git log -1 --oneline FETCH_HEAD)

echo 'Latest upstream commit: ' $LAST_UPSTREAM_COMMIT
echo 'Latest local commit: ' $LAST_DOWNSTREAM_COMMIT

test "$LAST_UPSTREAM_COMMIT" = "$LAST_DOWNSTREAM_COMMIT" && exit 0 # We are done

git reset --hard FETCH_HEAD

cat ../iris-fragment.yml >> src/gallium/drivers/iris/ci/gitlab-ci.yml
cat ../panfrost-fragment.yml >> src/gallium/drivers/panfrost/ci/gitlab-ci.yml
cat ../freedreno-fragment.yml >> src/freedreno/ci/gitlab-ci.yml
cat ../radeonsi-fragment.yml >> src/radeonsi/ci/gitlab-ci.yml
git commit -a -m 'ci: Add performance tracking jobs'

git push --force https://tomeu:${GITLAB_WRITE_TOKEN}@gitlab.freedesktop.org/tomeu/mesa.git HEAD:ci-performance-tracking

echo "TARGET_COMMIT=$(git rev-parse HEAD)" >> ../build.env

cd ..
