#!/usr/bin/python3

# Copyright (c) 2020 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

import gitlab
import influxdb_client
import os
import sys
import time
import dateutil.relativedelta
import datetime

from dateutil import parser

GITLAB_URL = os.environ.get('GITLAB_URL', 'https://gitlab.freedesktop.org')
GITLAB_PROJECT_PATH = os.environ.get('GITLAB_PROJECT_PATH', 'mesa/mesa')
GITLAB_TOKEN = os.environ.get('GITLAB_TOKEN', '<token unspecified>')
INFLUX_URL= os.environ.get('INFLUX_URL', 'https://influx-packet.freedesktop.org')
INFLUX_ORG = os.environ.get('INFLUX_ORG', 'Freedesktop')
INFLUX_BUCKET = os.environ.get('INFLUX_BUCKET', 'ci-metrics')
STATS_INFLUX_TOKEN = os.environ.get('STATS_INFLUX_TOKEN', '<token unspecified>')

def get_last_write(influx):
    query = """from(bucket:"{}") 
               |> range(start: -1y)
               |> drop(columns: ["job_name", "pipeline"])
               |> filter(fn:(r) => r._measurement == "job_failure")
               |> sort(columns:["_time"])
               |> last()"""
    query = query.format(INFLUX_BUCKET)
    result = influx.query(query, org=INFLUX_ORG)
    if len(result) == 0:
        return datetime.datetime.now(datetime.timezone.utc) - dateutil.relativedelta.relativedelta(months=1)
    return result[0].records[0].get_time()

def write_pipeline_failure(influx, pipeline_time, pipeline_id):
    print("Writing pipeline %s" % pipeline_time)

    p = influxdb_client.Point("pipeline_failure")
    p = p.tag("pipeline", pipeline_id)
    p = p.field("value", 1)
    p = p.time(pipeline_time)
    influx.write(bucket=INFLUX_BUCKET,
                 org=INFLUX_ORG,
                 record=p)

def write_job_failure(influx, job_time, job_name, job_stage, pipeline_id):
    print("Writing job %s" % job_time)

    p = influxdb_client.Point("job_failure")
    p = p.tag("job_name", job_name)
    p = p.tag("job_stage", job_stage)
    p = p.tag("pipeline", pipeline_id)
    p = p.field("value", 1)
    p = p.time(job_time)
    influx.write(bucket=INFLUX_BUCKET,
                 org=INFLUX_ORG,
                 record=p)

gl = gitlab.Gitlab(url=GITLAB_URL, private_token=GITLAB_TOKEN)
project = gl.projects.get(GITLAB_PROJECT_PATH)

influx_client = influxdb_client.InfluxDBClient(
    url=INFLUX_URL,
    token=STATS_INFLUX_TOKEN,
    org=INFLUX_ORG
)

since = get_last_write(influx_client.query_api())

for pipeline in project.pipelines.list(as_list=False, per_page=50, username="marge-bot"):
    created_at = parser.parse(pipeline.created_at)
    if created_at <= since:
        break
    print("Processing pipeline %d created at %s" % (pipeline.id, pipeline.created_at))
    pipeline_failed = False
    with influx_client.write_api() as write_api:
        for job in pipeline.jobs.list(as_list=False, per_page=150):
            job_name = job.name
            job_name_split = job_name.split(' ')
            if '/' in job_name_split[-1]:
                job_name = ' '.join(job_name_split[:-1])
            if job.status == 'failed':
                pipeline_failed = True
                write_job_failure(write_api, created_at, job_name, job.stage, pipeline.id)
        if pipeline_failed:
            write_pipeline_failure(write_api, created_at, pipeline.id)
        write_api.flush()
